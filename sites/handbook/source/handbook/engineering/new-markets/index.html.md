---
layout: handbook-page-toc
title: New Markets Department
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## New Markets Department

The New Markets Department within Engineering focuses on projects that are pre-Product/Market fit. The name "new markets" comes from one of our [three product investment types](/handbook/product/investment/#investment-types). They are ideas that may contribute to our revenue in 3-5 years time. Their focus should be to move fast, ship, get feedback, and [iterate](/handbook/values/#iteration). But first they've got to get from o to 1 and get something shipped.

We utilize [Single-engineer Groups](/company/team/structure/#single-engineer-groups) to draw benefits, but not distractions, from within the larger company and [GitLab project](https://gitlab.com/gitlab-org/gitlab) to maintain that focus. The Single-engineer group encompasses all of product development (product management, engineering, design, and quality) at the smallest scale. They are free to learn from, and collaborate with, those larger departments at GitLab but not at the expense of slowing down unnessarily.

The Department Head is the [VP of New Markets](/job-families/engineering/vp-of-new-markets/).

## Single-Engineer Groups

- ML OPs / GitLab Data
- Monitor APM
- 5 min prod app
- Real-time Collaboration

