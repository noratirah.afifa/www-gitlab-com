---
layout: markdown_page
title: "GitLab Legal"
description: "This page lists the subprocessors that GitLab may use."
---
{::options parse_block_html="true" /}
## Sub-processors of GitLab

Gitlab may engage the following entities to carry out specific processing activities on behalf of the customer.

[Sign Up](#sign-up) to be notified of a change to the list.

### GitLab Affiliates

| Affiliate Entity |Location|Service Provided|
|---|---|---|
|GitLab BV|Netherlands|Support Services|
|GitLab IT BV|Netherlands|Support Services|
|GitLab Ltd|United Kingdom|Support Services|
|GitLab Ireland Ltd|Ireland|Support Services|
|GitLab GmbH|Germany|Support Services|
|GitLab Pty Ltd|Australia|Support Services|
|GitLab Canada Corp.|Canada|Support Services|
|GitLab GK|Japan|Support Services|
|GitLab South Korea|South Korea|Support Services

### Third Party Sub-processors

|Third Party Entity|Location|Service Provided|
|---|---|---|
|Google LLC|United States|Cloud Hosting|
|Amazon Web Services Inc. |United States|Cloud Hosting|
|Rackspace Inc.|United States|Cloud Hosting Support|
|Zendesk Inc.|Germany|Support Services|
|Elasticsearch Inc.|United States|Search Functionality|

### Sign Up 
Complete this form to be notified of changes to our sub-processors.

<script src="//app-ab13.marketo.com/js/forms2/js/forms2.min.js"></script>

<form id="mktoForm_2833"></form>

<script>MktoForms2.loadForm("//app-ab13.marketo.com", "194-VVC-221", 2833);</script>

{::options parse_block_html="false" /}

